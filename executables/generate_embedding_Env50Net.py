import sys
sys.path.append("..")

from services.env_sound_service import EnvironmentalSoundServices


if __name__ == '__main__':
    serv = EnvironmentalSoundServices()
    serv.init_manager()
    serv.generate_embeddings()