import glob
import numpy as np
from sklearn.model_selection import train_test_split
from services.env_sound_service import EnvironmentalSoundServices
from helper.utilities import get_random_batch

serv = EnvironmentalSoundServices()
serv.init_manager()
env_sound_analysis = serv.manager
env_sound_analysis.init_env50Net_object()
env50Net = env_sound_analysis.env50Net

env_sound_analysis .load_k_fold_data()

y_data = np.concatenate(env_sound_analysis.y_data_fold, axis=0)
x_data = np.concatenate(env_sound_analysis.x_data_fold, axis=0)

# Separate data into
xTrain, xTest, yTrain, yTest = train_test_split(x_data, y_data, test_size = 0.2, random_state = 0)

# Get unique categories
env_sound_analysis.get_categories()

# Generate random pairs of data
# reorganize by groups
train_groups = [xTrain[np.where(yTrain==i)[0]] for i in np.unique(yTrain)]
test_groups = [xTest[np.where(yTest==i)[0]] for i in np.unique(yTest)]

input_shape = env_sound_analysis.raw_shape

# Start model
env50Net.reset_tf_session()
with env50Net.session.as_default():
    with env50Net.graph.as_default():
        if env50Net.embedding_model is None:
            env50Net.load_ENV50Net_embedding()
            env50Net.build_siamese_Env50Net(input_shape=input_shape)

            siamese_model = env50Net.siamese_model
            siamese_model.summary()

            # Compile Model
            env50Net.compile_siamese_Env50Net()

            # make a generator out of the data
            def siam_gen(in_groups, batch_size=128):
                while True:
                    pv_a, pv_b, pv_sim = get_random_batch(in_groups, batch_size//2)
                    yield [pv_a, pv_b], pv_sim


            # Validate the model
            batch_size = 258
            epochs = 100
            print("Training ...")
            valid_a, valid_b, valid_sim = get_random_batch(test_groups, batch_size)
            loss_history = siamese_model.fit_generator(siam_gen(train_groups, batch_size),
                                                       steps_per_epoch=500,
                                                       validation_data=([valid_a, valid_b], valid_sim),
                                                       epochs=epochs,
                                                       verbose=True)
            print("Done")