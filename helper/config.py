import os
from helper.custom_log import init_logger
from pathlib import Path

# Host/Port
host = "192.168.0.22"
port = "5001"

# Directories
parent_dir = "E:\\David\\Datasets\\ESC-50-master"
if not "executables" in str(os.getcwd()):
    service_parent_dir = os.getcwd()
else:
    service_parent_dir = Path(os.getcwd()).parent

normalize_spectrogram = False
if normalize_spectrogram:
    normalize_name = "_normalize"
else:
    normalize_name = "_non_normalize"

#"C:\\Users\\dmarg\\Desktop\\David\\Datasets\\ESC-50"
#os.path.join("E:\\David\\Datasets\\ESC-50-master")

filename_dataset = os.path.join(parent_dir, "meta", "esc50.csv")
k_fold_directory_output = os.path.join(parent_dir, "data_cross_validation" + normalize_name)
audio_dir = os.path.join(parent_dir, "audio")

# Service Names
main_service = 'Environmental Sound Event Classifier'
generate_csv_folder_task = 'CV Data Organization'
generate_dataset_task_name = 'Dataset Generation'
organize_cv_folders = 'K-Fold Folder Organization'

train_task_name = 'Train ESC-50 Model'
predict_task_name = 'Predict using EnvNet50 Model'
embedding_generation_task = 'Embedding Generation'
error_msg = 'An error occurred when launching the service. Error: '
success_msg = "The process ended with success!"

# Audio Parameters

spec_type = 'mel'
sr = 32000
n_mels = 2**6
n_fft = 2048
power = 2.0
fmax = int(sr/2)
fmin = 20
duration = 1 # Seconds
frame_length = int(duration*sr)
hop_length = 256


response = "target"
filename_col = "filename"
category_col = "category"
fold_col = "fold"


# Train/Val/Test parameters
data_augmentation = True
streech = True
pitch = False
noise = True
rate = [1.07, 0.81]
n_steps = [2.0, 2.5]
noise_range = 2

# Mahalanobis significance level
significance_level = 0.01

# Deep Learning Parameters
metric = "accuracy"
loss = "categorical_crossentropy" # kullback_leibler_divergence
epochs = 120
batch_size = 128
embedding_layer = 'embedding'
output_act = "softmax"
hidden_act = "relu"
optimizer_name = "adam"

# Directories
models_output_parent_dir = "models_output" + normalize_name
embedding_dir = "embeddings"
model_trained_dir = "trained_models"
model_trained_history_dir = "trained_models_history"
model_eval_dir = "trained_models_results"

# Filenames
model_name = "Env50Net_2.json"
model_history_name = model_name.replace(".json", "_history.json")
weight_data_name = model_name.replace(".json", "_weights.h5")
evaluation_results_name = model_name.replace(".json", "_eval_results.json")
model_plot_name = model_name.replace(".json", ".png")
embedding_name = 'emb_'
centroid_emb_name = 'centroid_emb_'
new_class_label = "unknown"

# Logging
logger = init_logger(__name__, testing_mode=False)