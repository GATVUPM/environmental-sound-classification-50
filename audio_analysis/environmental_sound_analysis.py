import os
import speechpy
import numpy as np
from audio_analysis.audio_preprocessing import AudioPreprocessing
from audio_analysis.deep_env50_net import Env50Net
from helper import config as cfg
from helper.utilities import (read_csv_file, normVec, mahalanobis,
                              get_pvalue_chi2, get_chi2_critical_value,
                              unison_shuffled_copies)
from sklearn.metrics.pairwise import cosine_similarity
from tqdm import tqdm


class EnvSoundManager:
    def __init__(self):

        # Config Parameters
        self.dataset_file = cfg.filename_dataset
        self.audio_dir = cfg.audio_dir
        self.dataset_df = None
        self.category_col = cfg.category_col
        self.target_col = cfg.response
        self.filename_col = cfg.filename_col
        self.fold_col = cfg.fold_col
        self.k_fold_directory_output = cfg.k_fold_directory_output
        self.categories = None
        self.categories_keys = None

        # Audio Parameters
        self.spec_type = cfg.spec_type
        self.sr = cfg.sr
        self.n_mels = cfg.n_mels
        self.n_fft = cfg.n_fft
        self.hop_length = cfg.hop_length
        self.power = cfg.power
        self.fmax = cfg.fmax
        self.fmin = cfg.fmin
        self.frame_length = cfg.frame_length
        self.data_augmentation = cfg.data_augmentation
        self.apply_strech = cfg.streech
        self.apply_pitch_mod = cfg.pitch
        self.apply_noise = cfg.noise
        self.normalize_spectrogram = cfg.normalize_spectrogram
        # Model Parameters
        self.output_dim = -1
        self.output_act = cfg.output_act
        self.hidden_act = cfg.hidden_act
        self.embedding_layer = cfg.embedding_layer
        self.optimizer_name = cfg.optimizer_name
        self.batch_size = cfg.batch_size
        self.epochs = cfg.epochs
        self.parent_dir = cfg.service_parent_dir

        self.models_output_parent_dir = cfg.models_output_parent_dir
        self.embedding_dir = cfg.embedding_dir
        self.model_trained_dir = cfg.model_trained_dir
        self.model_trained_history_dir = cfg.model_trained_history_dir
        self.model_eval_dir = cfg.model_eval_dir
        self.embedding_name = cfg.embedding_name
        self.centroid_emb_name = cfg.centroid_emb_name
        self.new_class_label = cfg.new_class_label


        self.model_name = cfg.model_name
        self.model_history_name = cfg.model_history_name
        self.weight_data_name = cfg.weight_data_name
        self.evaluation_results_name = cfg.evaluation_results_name
        self.embedding_dir_name = cfg.embedding_dir
        self.metric = cfg.metric
        self.loss = cfg.loss

        self.k_fold = -1
        self.model = None
        self.raw_shape = None

        # Training/Validation data
        self.x_data_fold = []
        self.y_data_fold = []
        self.x_train = None
        self.y_train = None
        self.x_val = None
        self.y_val = None

        self.total_dist = {}
        self.centroid_embs = np.array([])
        self.significance_level = cfg.significance_level

        # Custom objects
        self.ap = None
        self.env50Net = None
        self.init_preprocessing()

        # Service Parameters
        self.service_name = cfg.main_service
        self.status = None
        self.output = {}
        self.smooth_output = []

    def init_preprocessing(self):
        try:
            self.ap = AudioPreprocessing(spec_type=self.spec_type, sr=self.sr, n_mels=self.n_mels,
                                         n_fft=self.n_fft, hop_length=self.hop_length, power=self.power,
                                         fmax=self.fmax, frame_length=self.frame_length, response=self.target_col,
                                         filename_col=self.filename_col, data_augmentation=self.data_augmentation,
                                         apply_strech = self.apply_strech, apply_pitch_mod=self.apply_pitch_mod,
                                         apply_noise = self.apply_noise,
                                         k_fold_directory_output=self.k_fold_directory_output)
        except Exception as e:
            cfg.logger(e)

    def init_env50Net_object(self):
        try:
            self.env50Net = Env50Net(input_shape=self.raw_shape, metric=self.metric, loss=self.loss,
                                     output_dim=self.output_dim, output_act=self.output_act,
                                     hidden_act=self.hidden_act, embedding_layer=self.embedding_layer,
                                     optimizer_name=self.optimizer_name, epochs=self.epochs,
                                     batch_size=self.batch_size, parent_dir=self.parent_dir,
                                     models_output_parent_dir = self.models_output_parent_dir,
                                     embedding_dir=self.embedding_dir, model_trained_dir = self.model_trained_dir,
                                     model_eval_dir=self.model_eval_dir,
                                     model_trained_history_dir=self.model_trained_history_dir, model_name=self.model_name,
                                     model_history_name=self.model_history_name, weight_data_name=self.weight_data_name,
                                     evaluation_results_name=self.evaluation_results_name, embedding_filename=self.embedding_name,
                                     centroid_emb_filename=self.centroid_emb_name)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def get_categories(self):
        try:
            if self.dataset_df is None:
                self.dataset_df = read_csv_file(filename=self.dataset_file)
            self.categories = np.unique(self.dataset_df[self.target_col].values.tolist())
            self.categories_names = np.unique( self.dataset_df[self.category_col].values.tolist())
            self.categories_keys = dict(zip(self.categories, self.categories_names))
            self.output_dim = np.size(self.categories)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def build_dataset(self):
        d = []
        self.status = cfg.success_msg
        try:
            # Read data
            self.dataset_df = read_csv_file(filename=self.dataset_file)
            with tqdm(total=self.dataset_df.shape[0]) as pbar:
                for index, row in self.dataset_df.iterrows():
                    pbar.update(1)
                    try:
                        # Retrieve Data
                        audio_file = row[self.filename_col]
                        fold = row[self.fold_col]
                        target = row[self.target_col]
                        audio_path = os.path.join(self.audio_dir, audio_file)

                        # Load data
                        s = self.ap.load_audio_file(file_path=audio_path)
                        signal_preemphasized = speechpy.processing.preemphasis(s, cof=0.98)

                        # Remove initial silence from audio
                        s_n = self.ap.trim_audio_signal(x=signal_preemphasized)

                        done = True
                        offset = 0
                        # Overlapped window
                        while done:
                            done, frame = self.ap.analyze_audio_frame(s=s_n, offset=offset)
                            offset += int(self.frame_length / 2)
                            frame_normalize = self.ap.normalize_audio_segment(frame=frame, frame_length=self.frame_length)
                            spectrogram = self.ap.extract_features(frame_normalize, normalize=self.normalize_spectrogram)
                            if spectrogram is not None:
                                # Save Original Signal
                                d.append((spectrogram, target))
                            if self.data_augmentation:
                                ss_augmentated = self.ap.apply_data_augmentation(frames=frame_normalize,normalize=self.normalize_spectrogram)
                                if ss_augmentated:
                                    for y_changed_s in ss_augmentated:
                                        d.append((y_changed_s, target))
                            # Save data
                            self.ap.save_data_locally(data=d, k=fold)
                            d = []
                    except Exception as e:
                        cfg.logger.error(e)
                        continue
        except Exception as e:
            cfg.logger.error(e)
            self.status = cfg.error_msg + str(e)
        return self

    def train_env50_model(self):
        try:
            # load data
            self.load_k_fold_data()
            # Get unique categories
            self.get_categories()

            # Init object
            if self.env50Net is None:
                self.init_env50Net_object()
                self.env50Net.reset_tf_session()

            with self.env50Net.session.as_default():
                with self.env50Net.graph.as_default():
                    # Build Model
                    self.env50Net.build_ENV50Net()

                    # Plot model
                    # self.env50Net.plot_Env50Net()

                    # Compile Model
                    self.env50Net.compile_ENV50Net()
                    for index in range(self.k_fold):
                        # Prepare data
                        self.prepare_data_for_training(index=index)

                        # Train model
                        self.env50Net.train_ENV50Net(x_train=self.x_train, y_train=self.y_train,
                                                     x_val=self.x_val, y_val=self.y_val,
                                                     unique_categories=self.categories,
                                                     n_classes=self.output_dim)
                        # Save History
                        self.env50Net.save_history_ENV50Net()

                        # Save Metrics
                        self.env50Net.save_evaluation_metrics(k_fold=index)
        except Exception as e:
            cfg.logger.error(e)
            self.status = cfg.error_msg + str(e)
        return self

    def prepare_data_for_training(self, index):
        try:
            self.x_train = np.array([])
            self.y_train = np.array([])
            cfg.logger.info("Training/Validation on fold %s/%s", index + 1, self.k_fold)
            # Load only the data corresponding to these indices
            train_idx = [x for x in range(self.k_fold) if x != index]

            # Training data
            for ii, val in enumerate(train_idx):
                cfg.logger.info('Loading training data from index: %s', val)
                x_train_temp = self.x_data_fold[val]
                y_train_temp = self.y_data_fold[val]
                if ii == 0:
                    self.x_train = x_train_temp
                    self.y_train = y_train_temp
                else:
                    self.x_train = np.vstack((self.x_train, x_train_temp))
                    self.y_train = np.vstack((self.y_train, y_train_temp))

            # Validation data
            self.x_val = self.x_data_fold[index]
            self.y_val = self.y_data_fold[index]

            self.x_train = self.x_train[:, :, :, np.newaxis]
            self.x_val = self.x_val[:, :, :, np.newaxis]

            # Shuffle data
            self.x_train, self.y_train = unison_shuffled_copies(self.x_train, self.y_train)
            self.x_val, self.y_val = unison_shuffled_copies(self.x_val, self.y_val)

        except Exception as e:
            cfg.logger.error(e)
        return self

    def load_k_fold_data(self):
        try:
            if self.dataset_df is None:
                self.dataset_df = read_csv_file(filename=self.dataset_file)
            self.k_fold = max(list(set((self.dataset_df[self.fold_col].values.tolist()))))
            for kk in range(1, self.k_fold+1):
                cfg.logger.info('Loading data for k=%s/%s', str(kk), str(self.k_fold))
                x_train, y_train = self.ap.load_dataset(k=kk)
                self.x_data_fold.append(x_train)
                self.y_data_fold.append(y_train)
            # Adequate Shapes
            self.raw_shape = self.x_data_fold[0].shape[1:] + (1,)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def predict_env50(self, audio_file):
        try:
            # Init output
            non_smooth_output = {}
            non_smooth_output['sound_classification'] = []
            # Load data
            s = self.ap.load_audio_file(file_path=audio_file)

            # Remove initial silence from audio
            s_n = self.ap.trim_audio_signal(x=s)

            # Init model
            if self.env50Net is None:
                self.init_env50Net_object()
                self.env50Net.reset_tf_session()

            # Compute distances between embeddings and centroids
            if self.total_dist:
                self.compute_embedding_distances()

            done = True
            offset = 0
            # Overlapped window
            while done:
                t_init = round(offset/self.sr,2)
                t_end = round(t_init + (self.frame_length/self.sr),2)

                done, frame = self.ap.analyze_audio_frame(s=s_n, offset=offset)
                offset += int(self.frame_length / 2)
                frame_normalize = self.ap.normalize_audio_segment(frame=frame, frame_length=self.frame_length)
                spectrogram = self.ap.extract_features(frame_normalize, normalize=self.normalize_spectrogram)
                ss = spectrogram[:, :, :, np.newaxis]

                with self.env50Net.session.as_default():
                    with self.env50Net.graph.as_default():
                        if self.env50Net.model is None:
                            self.env50Net.load_ENV50Net()
                        if self.env50Net.embedding_model is None:
                            self.env50Net.load_ENV50Net_embedding()

                        self.model = self.env50Net.model
                        # Predict using the whole model
                        y_prob = self.model.predict(ss)

                        # Embedding matching
                        self.model = self.env50Net.embedding_model
                        x_emb = self.model.predict(ss)
                # Embedding Matching
                y_pred = np.argmax(y_prob)
                # Generate output
                output = {'start': t_init, 'end': t_end, 'event': y_pred}
                non_smooth_output['sound_classification'].append(output)

            # Smooth output
            self.generate_smooth_output(output=non_smooth_output)
        except Exception as e:
            cfg.logger.error(e)
            self.status = cfg.error_msg + str(e)
        return self

    def generate_smooth_output(self, output):
        try:
            if self.categories_keys is None:
                self.get_categories()

            last_pred = output['sound_classification'][0]['event']
            for i, data in enumerate(output['sound_classification']):
                if last_pred == data['event'] and i > 0:
                    output = {'start': output['sound_classification'][i-1]['start'],
                              'end': output['sound_classification'][i]['end'],
                              'event': self.categories_keys[data['event']]}
                else:
                    output = {'start': output['sound_classification'][i]['start'],
                              'end': output['sound_classification'][i]['end'],
                              'event': self.categories_keys[data['event']]}
                # Update temp variable
                last_pred = data
                self.smooth_output.append(output)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def generate_sound_embeddings(self):
        try:
            # Prepare object
            if self.env50Net is None:
                self.init_env50Net_object()
            if self.categories_keys is None:
                self.get_categories()
            if self.env50Net.session is None:
                self.env50Net.reset_tf_session()

            # Load Data
            self.load_k_fold_data()
            # Total number of speakers
            if isinstance(self.y_data_fold, list):
                self.y_data_fold = np.concatenate(self.y_data_fold, axis=0)

            y = [i[0] for i in self.y_data_fold.tolist()]

            # Check object type
            if isinstance(self.x_data_fold, list):
                x = np.concatenate(self.x_data_fold, axis=0)
            else:
                x = self.x_data_fold

            total_categories = len(self.categories_keys)

            # Get all the indexes associated to each category
            ind = {x: [i for i, value in enumerate(y) if value == x] for x in list(self.categories_keys.keys())}

            with tqdm(total=len(self.categories_names)) as pbar:
                cfg.logger.info('Generating embeddings ...')
                for i, cat in enumerate(list(set(y))):
                    pbar.update(1)
                    x_s = x[ind[cat]]
                    # Adequate the input shape
                    x_s = x_s[:, :, :, np.newaxis]
                    with self.env50Net.session.as_default():
                        with self.env50Net.graph.as_default():
                            if self.model is None:
                                self.env50Net.load_ENV50Net_embedding()
                                self.model = self.env50Net.embedding_model

                            # Predict
                            x_emb = self.model.predict(x_s)
                            # Normalize vector
                            x_emb = np.array([normVec(i) for i in x_emb])

                            # Get centroid embedding
                            x_cent_emb = np.mean(x_emb, axis=0)

                            # Get Category name
                            cat_name = self.categories_keys[cat]

                            # Save embedding
                            self.env50Net.save_sound_embedings(x_emb, x_cent_emb, cat_name)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def compute_embedding_distances(self):
        try:
            cfg.logger.info('Computing distances among embeddings and centroids ... ')
            centroid_embs = np.array([])
            with tqdm(total=len(self.categories_names)) as pbar:
                for i, cat in enumerate(self.categories_names):
                    pbar.update(1)
                    embds = self.env50Net.load_sound_embedding(cat_name=cat)
                    c_emb = self.env50Net.load_centroid_embedding(cat_name=cat).reshape((1, embds.shape[1]))

                    degrees_freedom = embds.shape[1]

                    # Compute Mahalanobis distance
                    critical_value = get_chi2_critical_value(significance_level=self.significance_level,
                                                             degrees_freedom=degrees_freedom)

                    mahalanobis_dist = mahalanobis(c_emb, embds)

                    pvalue = get_pvalue_chi2(mahalanobis_dist, degrees_freedom)

                    dist_data = {'mahalanobis_dist': mahalanobis_dist, 'pvalue':pvalue,
                                 'critical_value': critical_value}
                    # Save data
                    self.total_dist[cat] = dist_data

                    # Save centroids
                    if i == 0:
                        centroid_embs = c_emb
                    else:
                        centroid_embs = np.concatenate([centroid_embs, c_emb], axis=0)
            self.centroid_embs = centroid_embs
        except Exception as e:
            cfg.logger.error(e)
        return self

    def embedding_matching(self, x_new_emb):
        predicted_speaker = -1
        try:
            cfg.logger.info('Applying Embedding Matching to retrieve the best candidate')
            critical_value = None
            with tqdm(total=len(self.categories_names)) as pbar:
                for i, cat in enumerate(self.categories_names):
                    pbar.update(1)
                    embds = self.env50Net.load_sound_embedding(cat_name=cat)
                    degrees_freedom = embds.shape[1]

                    # Compute Mahalanobis distance
                    if critical_value is None:
                        critical_value = get_chi2_critical_value(significance_level=self.significance_level,
                                                                 degrees_freedom=degrees_freedom)

                    mahalanobis_dist = mahalanobis(x_new_emb, embds)[0]

                    pvalue = get_pvalue_chi2(mahalanobis_dist, degrees_freedom)

                    dist_data = {'mahalanobis_dist': mahalanobis_dist, 'pvalue': pvalue,
                                 'critical_value': critical_value}
                    # Save data
                    self.total_dist[cat] = dist_data

            # Check the distance

            # Threshold (compare the result with the maximum distance)
            if 2 >= (self.total_max[9] + self.total_sd[9]):
                predicted_speaker = self.new_class_label
            else:
                predicted_speaker = self.categories_keys[9]

        except Exception as e:
            cfg.logger.error(e)
        return predicted_speaker