import os
import operator
import tensorflow as tf
import numpy as np
from keras.models import Sequential, model_from_json, Model
from keras.layers import Dense, Dropout, Flatten, Activation, Input,Lambda
from keras.layers.convolutional import Conv2D
from keras.layers.pooling import MaxPooling2D
from keras import backend as K
from keras.callbacks import EarlyStopping
from keras import optimizers
from keras.utils import to_categorical, plot_model
from helper import config as cfg
from sklearn.utils import class_weight
from helper.utilities import (write_json, generate_json_format,
                              read_json, prepare_directory,read_numpy_file,
                              write_numpy_file,update_path_name_by_string)


class Env50Net:
    def __init__(self, input_shape, metric, loss, output_dim, output_act, hidden_act, embedding_layer,
                 optimizer_name, epochs, batch_size, parent_dir, models_output_parent_dir, embedding_dir,
                 model_trained_dir, model_eval_dir, model_trained_history_dir, model_name,
                 model_history_name, weight_data_name, evaluation_results_name, embedding_filename,
                 centroid_emb_filename):

        self.input_shape = input_shape
        self.metrics = [metric]
        self.loss = loss
        self.siamese_loss = "binary_crossentropy"
        self.output_dim = output_dim
        self.output_act = output_act
        self.hidden_act = hidden_act
        self.filter_size = None
        self.kernel_size = None
        self.pool_size = None
        self.embedding_layer = embedding_layer
        self.optimizer_name = optimizer_name
        self.optimizer = None
        self.padding = "valid"
        self.init_model = "he_normal"
        self.callback_elements = []
        self.epochs = epochs
        self.batch_size = batch_size
        self.model = None
        self.siamese_model = None
        self.embedding_model = None
        self.model_history = None
        self.parent_dir = parent_dir

        self.models_output_parent_dir = os.path.join(self.parent_dir, models_output_parent_dir)
        self.embedding_dir = os.path.join(self.models_output_parent_dir, embedding_dir)
        self.model_trained_dir = os.path.join(self.models_output_parent_dir, model_trained_dir)
        self.model_eval_dir = os.path.join(self.models_output_parent_dir, model_eval_dir)
        self.model_trained_history_dir = os.path.join(self.models_output_parent_dir, model_trained_history_dir)

        self.model_path = os.path.join(self.model_trained_dir, model_name)
        self.model_history_path = os.path.join(self.model_trained_history_dir, model_history_name)
        self.weight_path = os.path.join(self.model_trained_dir, weight_data_name)
        self.evaluation_results_path = os.path.join(self.model_eval_dir, evaluation_results_name)
        self.model_plot_name = cfg.model_plot_name

        self.embedding_name = embedding_filename
        self.centroid_emb_name = centroid_emb_filename

        self.graph = None
        self.session = None
        self.init_process()

    def init_process(self):
        try:
            prepare_directory(self.models_output_parent_dir)
            prepare_directory(self.embedding_dir)
            prepare_directory(self.model_trained_dir)
            prepare_directory(self.model_eval_dir)
            prepare_directory(self.model_trained_history_dir)
            self.extract_conv2D_params()
        except Exception as e:
            cfg.logger.error(e)
        return self


    def build_feature_extraction_model(self):
        try:
            pass
        except Exception as e:
            cfg.logger.error(e)
        return self

    def feature_extraction_model(self):
        try:
            self.model = Sequential()

            # TODO: Add VGG
        except Exception as e:
            cfg.logger.error(e)
        return self

    def build_ENV50Net(self):
        try:
            self.model = Sequential()

            # -------------------------------- Block One
            self.model.add(Conv2D(self.filter_size, self.kernel_size,
                                  padding=self.padding, input_shape=self.input_shape))
            self.model.add(Activation(self.hidden_act))
            self.model.add(Conv2D(self.filter_size, self.kernel_size, padding=self.padding))
            self.model.add(Activation(self.hidden_act))
            self.model.add(MaxPooling2D(pool_size=self.pool_size, padding=self.padding))
            self.model.add(Dropout(0.25))

            # Increase filter size
            self.update_filter_kernel_size(increase_filter=True, increase_kernel=None)

            # -------------------------------- Block Two
            self.model.add(Conv2D(self.filter_size, self.kernel_size, padding=self.padding))
            self.model.add(Activation(self.hidden_act))
            self.model.add(Conv2D(self.filter_size, self.kernel_size, padding=self.padding))
            self.model.add(Activation(self.hidden_act))
            self.model.add(MaxPooling2D(self.pool_size, padding=self.padding))
            self.model.add(Dropout(0.25))

            # Increase filter size
            self.update_filter_kernel_size(increase_filter=True, increase_kernel=None)

            # --------------------------------  Dense One
            self.model.add(Flatten())
            self.model.add(Dense(self.filter_size, name=self.embedding_layer))
            self.model.add(Activation(self.hidden_act))
            self.model.add(Dropout(0.5))

            # Increase filter size
            self.update_filter_kernel_size(increase_filter=False, increase_kernel=None)

            # --------------------------------  Dense Two
            self.model.add(Dense(self.filter_size))
            self.model.add(Activation(self.hidden_act))
            self.model.add(Dropout(0.5))

            # --------------------------------  Output Layer
            self.model.add(Dense(self.output_dim, activation=self.output_act))

            # Model Summary
            self.model.summary()

        except Exception as e:
            cfg.logger.error(e)
        return self

    def build_siamese_Env50Net(self, input_shape):
        try:
            input_left = Input(shape=input_shape)
            input_right = Input(shape=input_shape)
            encoded_left = self.embedding_model(input_left)
            encoded_right = self.embedding_model(input_right)

            # `encoder` is any predefined network that maps a single sample
            # into an embedding space.
            # `encoder` should take an input with shape (None,) + input_shape
            # and produce an output with shape (None, embedding_dim).
            # None indicates the batch dimension.

            # Here I calculate the euclidean distance between the two encoded
            # samples though other distances can be used
            """embedded_distance = Subtract()([encoded_1, encoded_2])
            embedded_distance = Lambda(
                lambda x: K.sqrt(K.mean(K.square(x), axis=-1, keepdims=True))
            )(embedded_distance)"""

            # Add a customized layer to compute the absolute difference between the encodings
            L1_layer = Lambda(lambda tensors: K.abs(tensors[0] - tensors[1]))
            embedded_distance = L1_layer([encoded_left, encoded_right])

            # Add a dense+sigmoid layer here in order to use per-pair, binary
            # similar/dissimilar labels
            output = Dense(1, activation='sigmoid')(embedded_distance)
            self.siamese_model = Model(inputs=[input_left, input_right], outputs=output)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def extract_conv2D_params(self):
        try:
            # filters
            self.filter_size = 2**6
            self.pool_size = (2, 2)
            self.kernel_size = (3,3)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def update_filter_kernel_size(self, increase_filter=None, increase_kernel=None,
                                  step=1, kernel_step=(2, 2)):
        try:
            if increase_filter:
                self.filter_size = int(self.filter_size * (2**step))
            elif increase_filter is None:
                pass
            else:
                self.filter_size = int(self.filter_size / (2 ** step))

            if increase_kernel:
                self.kernel_size = tuple(map(operator.add,
                                             self.kernel_size, kernel_step))
            elif increase_kernel is None:
                pass
            else:
                self.kernel_size = tuple(map(operator.sub,
                                             self.kernel_size, kernel_step))
        except Exception as e:
            cfg.logger.error(e)
        return self

    def compile_ENV50Net(self):
        try:
            if self.optimizer is None:
                self.select_optimizer()
            self.metrics += [self.f1_m, self.precision_m, self.recall_m]
            cfg.logger.info('Compile model ... ')
            self.model.compile(loss=self.loss, optimizer=self.optimizer, metrics=self.metrics)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def compile_siamese_Env50Net(self):
        try:
            if self.optimizer is None:
                self.select_optimizer()
            cfg.logger.info('Compile model ... ')
            self.siamese_model.compile(loss=self.siamese_loss, optimizer=self.optimizer,
                                       metrics = ['mae'])
        except Exception as e :
            cfg.logger.error(e)
        return self

    def recall_m(self, y_true, y_pred):
        try:
            true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
            possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
            recall = true_positives / (possible_positives + K.epsilon())
        except Exception as e:
            print(e)
            recall = None
        return recall

    def precision_m(self, y_true, y_pred):
        try:
            true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
            predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
            precision = true_positives / (predicted_positives + K.epsilon())
        except Exception as e:
            print(e)
            precision = None
        return precision

    def cosine_distance(self, vectors):
        try:
            x,y = vectors
            x_n = K.l2_normalize(x, axis=-1)
            y_n = K.l2_normalize(y, axis=-1)
            dist = K.mean(1 - K.sum((x_n * y_n), axis=-1), axis=-1)
        except Exception as e:
            cfg.logger.error(e)
            dist = None
        return dist

    def cosine_dist_output_shape(self, shapes):
        try:
            shape1, shape2 = shapes
            #print((shape1[0], 1))
            output_shape = (shape1[0], 1)
        except Exception as e:
            cfg.logger.error(e)
            output_shape = None
        return output_shape

    def f1_m(self, y_true, y_pred):
        try:
            precision = self.precision_m(y_true, y_pred)
            recall = self.recall_m(y_true, y_pred)
        except Exception as e:
            print(e)
            return None
        return 2 * ((precision * recall) / (precision + recall + K.epsilon()))

    def select_optimizer(self):
        try:
            if self.optimizer_name == 'sgd':
                self.optimizer = optimizers.SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
            elif self.optimizer_name == 'adagrad':
                self.optimizer = optimizers.Adagrad(lr=0.01, epsilon=None, decay=0.0)
            elif self.optimizer_name == 'adadelta':
                self.optimizer = optimizers.Adadelta(lr=1.0, rho=0.95, epsilon=None, decay=0.0)
            elif self.optimizer_name == 'adam':
                self.optimizer = optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0,
                                                 amsgrad=False)
            elif self.optimizer_name == 'nadam':
                self.optimizer = optimizers.Nadam(lr=0.002, beta_1=0.9, beta_2=0.999, epsilon=None,
                                                  schedule_decay=0.004)
            else:
                cfg.logger.warning('Not valid optimizer!. Please check the configuration script.')
        except Exception as e:
            cfg.logger.error(e)
        return self

    def train_ENV50Net(self, x_train, y_train, x_val, y_val, unique_categories, n_classes):
        try:
            # Check y_train
            class_weights = class_weight.compute_class_weight('balanced', unique_categories,
                                                              y_train.flatten())

            # Categorical
            y_train_t = to_categorical(y_train, num_classes=n_classes)
            y_val_t = to_categorical(y_val, num_classes=n_classes)

            earlystop = EarlyStopping(monitor='val_acc',
                                      patience=4,
                                      verbose=1,
                                      mode='max')

            self.callback_elements.append(earlystop)
            self.model_history = self.model.fit(x_train, y_train_t, epochs=self.epochs,
                                                batch_size=self.batch_size, verbose=2,
                                                validation_data=(x_val, y_val_t),
                                                class_weight=class_weights,
                                                callbacks=self.callback_elements)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def save_history_speakerDirNet(self, k_fold=1):
        try:
            # Save model
            model_json = self.model_history.model.to_json()
            if k_fold is not None:
                self.model_path = update_path_name_by_string(full_path=self.model_path, index=os.sep,
                                                             substr='_', additional_str=str(k_fold),
                                                             extension='.json')
                self.weight_path = update_path_name_by_string(full_path=self.weight_path, index=os.sep,
                                                              substr='_', additional_str=str(k_fold),
                                                              extension='.h5')
                self.model_history_path = update_path_name_by_string(full_path=self.model_history_path,
                                                                     index=os.sep, substr='_',
                                                                     additional_str=str(k_fold),
                                                                     extension='.json')

            write_json(json_data=model_json, json_path=self.model_path)
            cfg.logger.info("Saved trained model at %s", self.model_path)

            # Save weights
            self.model_history.model.save_weights(self.weight_path)
            cfg.logger.info("Saved weights at %s", self.weight_path)

            # Save history
            history_json = generate_json_format(input=self.model_history.history)
            write_json(json_data=history_json, json_path=self.model_history_path)
            cfg.logger.info("Saved model history at %s", self.model_history_path)

        except Exception as e:
            cfg.logger.error(e)
        return self

    def save_evaluation_metrics(self, k_fold=1):
        output = {}
        try:
            for key, values in self.model_history.history.items():
                mean_key = key + "_mean"
                sd_key = key + "_sd"
                output[key] = {mean_key: np.mean(values), sd_key: np.std(values)}
            # Save Results
            eval_res_json = generate_json_format(output)
            if k_fold is not None:
                self.evaluation_results_path = update_path_name_by_string(full_path=self.evaluation_results_path,
                                                                          index=os.sep, substr='_',
                                                                          additional_str=str(k_fold),
                                                                          extension='.json')
            write_json(json_data=eval_res_json, json_path=self.evaluation_results_path)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def load_ENV50Net(self, k_fold=1):
        try:
            # Preprocess names
            self.model_path = self.model_path.replace('.json', '_' + str(k_fold) + '.json')
            self.weight_path = self.weight_path.replace('.h5', '_' + str(k_fold) + '.h5')

            architecture = read_json(json_path=self.model_path)
            architecture_json = generate_json_format(architecture)

            # Load model from json
            self.model = model_from_json(architecture_json)
            self.model.load_weights(self.weight_path)

        except Exception as e:
            cfg.logger.error(e)
        return self

    def reset_tf_session(self):
        try:
            # Reset graph
            if self.graph is not None:
                self.graph = None
            # Reset session
            if self.session is not None:
                self.session = None
            self.graph = tf.Graph()
            self.session = tf.Session(graph=self.graph)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def load_ENV50Net_embedding(self):
        try:
            # Load Model
            self.load_ENV50Net()
            self.embedding_model = Model(self.model.input,
                                         self.model.get_layer(self.embedding_layer).output)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def plot_Env50Net(self):
        try:
            # Plot architecture
            plot_model(self.model,to_file=self.model_plot_name, show_shapes=True,
                       show_layer_names=True, rankdir='TB')
        except Exception as e:
            cfg.logger.error(e)
        return self

    def save_sound_embedings(self, x_emb, x_cent_emb, cat_name):
        try:
            embedding_file = os.path.join(self.embedding_dir, self.embedding_name + str(cat_name))
            embedding_file_cent = os.path.join(self.embedding_dir, self.centroid_emb_name + str(cat_name))
            write_numpy_file(embedding_file, data=x_emb)
            write_numpy_file(embedding_file_cent, data=x_cent_emb)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def load_sound_embedding(self, cat_name):
        try:
            filename_emb = os.path.join(self.embedding_dir, self.embedding_name + str(cat_name) + '.npy')
            sound_embedding = read_numpy_file(numpy_filename=filename_emb)
        except Exception as e:
            cfg.logger.error(e)
            sound_embedding = None
        return sound_embedding

    def load_centroid_embedding(self, cat_name):
        try:
            filename_cent = os.path.join(self.embedding_dir, self.centroid_emb_name + str(cat_name) + '.npy')
            centroid_emb = read_numpy_file(numpy_filename=filename_cent)
        except Exception as e:
            cfg.logger.error(e)
            centroid_emb = None
        return centroid_emb