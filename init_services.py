from flask import Flask, request
from services.env_sound_service import EnvironmentalSoundServices
from helper import config as cfg
import json

app = Flask(__name__)

@app.route('/env_sound/generate_data', methods=['GET'])
def generate_dataset():
    serv = EnvironmentalSoundServices()
    serv.generate_data()
    output = serv.output
    return json.dumps(output)


@app.route('/env_sound/train', methods=['GET'])
def tut_urban_train():
    serv = EnvironmentalSoundServices()
    serv.train()
    output = serv.output
    return json.dumps(output)


@app.route('/env_sound/predict', methods=['POST'])
def sound_detector_start():
    params = request.get_json(force=True)
    serv = EnvironmentalSoundServices()
    output = serv.predict_env_sound(params=params)
    return json.dumps(output)


if __name__ == '__main__':
    app.run(host=cfg.host, port=cfg.port, debug=False)