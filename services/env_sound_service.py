from helper import config as cfg
from helper.utilities import build_output
from audio_analysis.environmental_sound_analysis import EnvSoundManager


class EnvironmentalSoundServices:
    def __init__(self):
        self.output = {}
        self.service_name = cfg.main_service
        self.manager = None

    def init_manager(self):
        try:
            self.manager = EnvSoundManager()
        except Exception as e:
            cfg.logger.error(e)
        return self

    def generate_data(self):
        task = cfg.generate_dataset_task_name
        try:
            # Cross validation process
            self.manager.build_dataset()
            status = self.manager.status
            self.output = build_output(name=self.service_name, task=task, status=status)
        except Exception as e:
            cfg.logger.error(e)
            status = 'An error occurred when generating the dataset. Error information: ' + str(e)
            self.output = build_output(name=self.service_name, task=task, status=status)
        return self

    def train(self):
        task = cfg.train_task_name
        try:
            self.manager.train_env50_model()
            status = self.manager.status
            self.output = build_output(name=self.service_name, task=task, status=status)
        except Exception as e:
            cfg.logger.error(e)
            status = 'An error occurred when training the model. Error information: ' + str(e)
            self.output = build_output(name=self.service_name, task=task, status=status)
        return self

    def predict_env_sound(self, params):
        task = cfg.predict_task_name
        try:
            audio_file = params["audio_file"]
            self.manager.predict_env50(audio_file=audio_file)
            data = self.manager.output
            status = self.manager.status
            self.output = build_output(name=self.service_name, task=task, status=status, data=data)
        except Exception as e:
            cfg.logger.error(e)
            status = 'An error occurred when predicting the audio. Error information: ' + str(e)
            self.output = build_output(name=self.service_name, task=task, status=status,data=[])
        return self
    
    def generate_embeddings(self):
        task = cfg.embedding_generation_task
        try:
            self.manager.generate_sound_embeddings()
            status = self.manager.status
            self.output = build_output(name=self.service_name, task=task, status=status)
        except Exception as e:
            cfg.logger.error(e)
            status = 'An error occurred when generating the embeddings. Error information: ' + str(e)
            self.output = build_output(name=self.service_name, task=task, status=status)
        return self
